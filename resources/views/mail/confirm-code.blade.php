Assalamu'alaikum {{ $user }},<br/>
Berikut adalah kode untuk aktifasi akun anda.
<br/><br/>
<b>{{ $code }}</b>
<br/><br/>
Kode ini hanya berlaku dalam waktu 30 menit