angular.module('PabApp')
  .directive('pin', function() {
    return {
      restrict: "A",
      link: function(scope, elem, attrs) {
        elem.on('keyup', function(e) {
          var partsId = attrs.id.match(/pin(\d+)/);
          var currentId = parseInt(partsId[1]);
          var key = event.keyCode || event.charCode;

          if( key == 8 || key == 46 ) {
            e.preventDefault();
            prevElement = document.querySelector('#pin' + (currentId - 1));
            if(prevElement != null){
              prevElement.focus();
              prevElement.select();
            }
          }

          var l = elem.val().length;
          if (l == elem.attr("maxlength")) {
            nextElement = document.querySelector('#pin' + (currentId + 1));
            if(nextElement == null) {
              submitElement = document.querySelector('#pin-submit');
              // submitElement.click();
            }
            else {
              nextElement.focus();
              nextElement.select();
            }
          }
        });
      }
    };
  });

