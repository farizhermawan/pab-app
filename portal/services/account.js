angular.module('PabApp')
  .factory('Account', function($http) {
    return {
      getProfile: function() {
        return $http.get('api/me');
      },
      updateProfile: function(profileData) {
        return $http.put('api/me', profileData);
      },
      sendCode: function() {
        return $http.get('api/me/sendcode');
      },
      activate: function(pin) {
        return $http.put('api/me/activate', {code: pin});
      },
      getPrograms: function(pin) {
        return $http.get('api/me/programs');
      }
    };
  });