angular.module('PabApp')
  .factory('Batch', function($http) {
    return {
      getAvailablePrograms: function() {
        return $http.get('api/batch/current/programs');
      },
      assignProgram: function(program) {
        return $http.post('api/batch/assign', program);
      }
    };
  });