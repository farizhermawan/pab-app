angular.module('PabApp')
  .factory('Common', function($http) {
    return {
      getProvinces: function($query) {
        return $http.get('api/provinces?q='+$query);
      },
      getRegencies: function($query, $parent) {
        return $http.get('api/regencies?q='+$query+'&p='+$parent);
      },
      getDistricts: function($query, $parent) {
        return $http.get('api/districts?q='+$query+'&p='+$parent);
      },
      getVillages: function($query, $parent) {
        return $http.get('api/villages?q='+$query+'&p='+$parent);
      }
    };
  });