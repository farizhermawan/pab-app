angular.module('PabApp')
  .controller('ProfileCtrl', function($scope, $filter, $auth, $q, $location, toastr, Account, Common) {
    $scope.changePasswordView = false;
    $scope.address = {
      province: null,
      regency: null,
      district: null,
      village: null
    };

    function calculateAge(birthday) {
      let ageDifMs = Date.now() - birthday.getTime();
      let ageDate = new Date(ageDifMs);
      return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    function afterInitUser() {
      if($scope.user.address_detail != null) {
        $scope.address = JSON.parse($scope.user.address_detail);
      }
      if($scope.user.birth_date != null) {
        var partDate = $scope.user.birth_date.split(" ")[0].split("-");
        $scope.user.birth_date_object = new Date();
        $scope.user.birth_date_object.setFullYear(partDate[0], partDate[1]-1, partDate[2]);
      }
      else $scope.user.birth_date_object = undefined;
    }

    $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          $scope.user = response.data;
          afterInitUser();
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };

    $scope.updateProfile = function() {
      var age = calculateAge($scope.user.birth_date_object);
      if(age > 28) {
        toastr.error("Maaf, umur anda melebihi dari persyaratan.");
        return;
      }
      else if(age < 15) {
        toastr.error("Maaf, umur anda kurang dari persyaratan.");
        return;
      }
      $scope.user.address_detail = JSON.stringify($scope.address);
      $scope.user.birth_date = $filter('date')($scope.user.birth_date_object, 'yyyy-MM-dd');
      Account.updateProfile($scope.user)
        .then(function(response) {
          let redirectToProgramPage = !$scope.user.isCompletedProfile && response.data.isCompletedProfile;
          toastr.success('Profile has been updated');
          if(redirectToProgramPage) $location.path('/program');
          else {
            $scope.user = response.data;
            afterInitUser();
          }
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.link = function(provider) {
      $auth.link(provider)
        .then(function() {
          toastr.success('You have successfully linked a ' + provider + ' account');
          $scope.getProfile();
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.unlink = function(provider) {
      $auth.unlink(provider)
        .then(function() {
          toastr.info('You have unlinked a ' + provider + ' account');
          $scope.getProfile();
        })
        .catch(function(response) {
          toastr.error(response.data ? response.data.message : 'Could not unlink ' + provider + ' account', response.status);
        });
    };

    $scope.getProvinces = function(query, querySelectAs) {
      let deferred = $q.defer();
      if(query.length === 0) deferred.resolve();
      else {
        Common.getProvinces(query)
          .then(function(respon) {
            deferred.resolve(respon.data);
          })
          .catch(function(response) {
            toastr.error(response.data.message, response.status);
          });
      }
      return deferred.promise;
    };

    $scope.getRegencies = function(query, querySelectAs) {
      let deferred = $q.defer();
      Common.getRegencies(query, $scope.address.province.id)
        .then(function(respon) {
          deferred.resolve(respon.data);
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
      return deferred.promise;
    };

    $scope.getDistricts = function(query, querySelectAs) {
      let deferred = $q.defer();
      Common.getDistricts(query, $scope.address.regency.id)
        .then(function(respon) {
          deferred.resolve(respon.data);
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
      return deferred.promise;
    };

    $scope.getVillages = function(query, querySelectAs) {
      let deferred = $q.defer();
      Common.getVillages(query, $scope.address.district.id)
        .then(function(respon) {
          deferred.resolve(respon.data);
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
      return deferred.promise;
    };

    $scope.getProfile();

  });
