angular.module('PabApp')
  .controller('BatchCtrl', function($scope, $location, $auth, $timeout, toastr, Account) {
    $scope.pin = [];
    $scope.isLoading = false;
    $scope.counter = 0;

    $scope.onTimeout = function(){
      if($scope.counter > 0) {
        $scope.counter--;
        $scope.stopWatch = $timeout($scope.onTimeout,1000);
      }
    };

    $scope.stop = function(){
      $timeout.cancel(mytimeout);
    };

    $scope.submitPin = function () {
      $scope.isLoading = true;
      Account.activate($scope.pinString())
        .then(function() {
          $scope.isLoading = false;
          toastr.success('Akun kamu telah aktif');
          $location.path('/');
        })
        .catch(function(response) {
          $scope.isLoading = false;
          toastr.error(response.data.message);
        })
    };

    $scope.pinString = function () {
      return $scope.pin.join("");
    };

    $scope.sendCode = function () {
      if($scope.counter === 0) {
        $scope.counter = 30;
        $scope.stopWatch = $timeout($scope.onTimeout,1000);
        Account.sendCode()
          .then(function () {
            toastr.info("Kode telah dikirimkan ke email anda");
          })
          .catch(function (response) {
            toastr.error(response.data.message);
          })
      }
    };

    Account.getProfile()
      .then(function(response) {
        $scope.user = response.data;
        if($scope.user.isActive) $location.path('/');
        else if(!$scope.user.confirmation_code) $scope.sendCode();
      })
      .catch(function(response) {
        toastr.error(response.data.message, response.status);
      });

  });