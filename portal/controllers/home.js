angular.module('PabApp')
  .controller('HomeCtrl', function($scope, $http, Account) {
    Account.getProfile()
      .then(function(response) {
        $scope.user = response.data;
      })
      .catch(function(response) {
        toastr.error(response.data.message, response.status);
      });
  });
