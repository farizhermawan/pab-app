angular.module('PabApp')
  .controller('NavbarCtrl', function($scope, $auth, Account) {
    $scope.mobileMenuIsOpen = false;

    $scope.toggleMobileMenu = function() {
      $scope.mobileMenuIsOpen = !$scope.mobileMenuIsOpen;
    };

    $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
    };

    $scope.isActivation = function () {
      return window.location.hash === "#/activation";
    };

    Account.getProfile()
      .then(function(response) {
        $scope.user = response.data;
      })
      .catch(function(response) {
        toastr.error(response.data.message, response.status);
      });
  });
