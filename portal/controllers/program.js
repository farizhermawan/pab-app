angular.module('PabApp')
  .controller('ProgramCtrl', function ($scope, $filter, $auth, $location, $q, toastr, Batch, Account, Upload) {

    $scope.selectedProgram = null;
    $scope.file = null;
    $scope.isLoaded = null;

    $scope.getAvailablePrograms = function () {
      Batch.getAvailablePrograms()
        .then(function (respon) {
          $scope.batch = respon.data;
        })
        .catch(function (response) {
          toastr.error(response.data.message, response.status);
        });
    };

    $scope.getMyPrograms = function () {
      Account.getPrograms()
        .then(function (respon) {
          $scope.myProgram = respon.data;
          if (!$scope.myProgram.isCompletedProfile) {
            toastr.info("Silahkan lengkapi profile anda untuk bisa melanjutkan pendaftaran.");
            $location.path('/profile');
          }
          else $scope.isLoaded = true;
        })
        .catch(function (response) {
          toastr.error(response.data.message, response.status);
        });
    };

    $scope.assignProgram = function (selectedProgram) {
      if (!selectedProgram) {
        return;
      }
      Batch.assignProgram(selectedProgram)
        .then(function (respon) {
          toastr.success("Pendaftaran program " + selectedProgram.name + " berhasil");
          $scope.getMyPrograms();
        })
        .catch(function (response) {
          toastr.error(response.data.message, response.status);
        });
    };

    $scope.uploadProofPayment = function (file) {
      if (file) $scope.upload(file);
    };

    $scope.upload = function (file) {
      Upload.upload({
        url: 'api/payment/upload',
        data: {
          file: file,
          amount: 0,
          program_id: $scope.myProgram.currentProgram.id
        }
      }).then(function (resp) {
        toastr.success('Upload bukti pembayaran berhasil. Kami akan segera mengkonfirmasi.');
        $scope.getMyPrograms();
      }, function (resp) {
        toastr.error(resp.data.message, resp.status);
      }, function (evt) {
        let progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
      });
    };

    $scope.getMyPrograms();
    $scope.getAvailablePrograms();

  });
