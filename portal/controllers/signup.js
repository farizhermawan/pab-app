angular.module('PabApp')
  .controller('SignupCtrl', function($scope, $location, $auth, toastr) {
    $scope.signup = function() {
      $auth.signup($scope.user)
        .then(function(response) {
          $auth.setToken(response);
          $location.path('/activation');
          toastr.info('Akun anda berhasil didaftarkan');
        })
        .catch(function(response) {
          if(response.data.message.email) {
            toastr.error(response.data.message.email[0]);
          }
          else toastr.error(response.data.message);
        });
    };
  });