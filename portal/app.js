angular.module('PabApp', ['ngResource', 'ngMessages', 'ngAnimate', 'toastr', 'ui.router', 'satellizer', 'pageslide-directive', 'oi.select', 'ngFileUpload'])
  .config(function($stateProvider, $urlRouterProvider, $authProvider) {

    /**
     * Helper auth functions
     */
    var skipIfLoggedIn = function($q, $auth) {
      var deferred = $q.defer();
      if ($auth.isAuthenticated()) {
        deferred.reject();
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    };

    var loginRequired = function($q, $location, $auth) {
      var deferred = $q.defer();
      if ($auth.isAuthenticated()) {
        deferred.resolve();
      } else {
        $location.path('/login');
      }
      return deferred.promise;
    };

    /**
     * App routes
     */
    $stateProvider
      .state('home', {
        url: '/',
        controller: 'HomeCtrl',
        templateUrl: 'partials/home.html',
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state('notfound', {
        url: '/404',
        templateUrl: 'partials/notfound.html',
      })
      .state('login', {
        url: '/login',
        templateUrl: 'partials/login.html',
        controller: 'LoginCtrl',
        resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'partials/signup.html',
        controller: 'SignupCtrl',
        resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
      })
      .state('activation', {
        url: '/activation',
        templateUrl: 'partials/activation.html',
        controller: 'ActivationCtrl',
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state('logout', {
        url: '/logout',
        template: null,
        controller: 'LogoutCtrl'
      })
      .state('profile', {
        url: '/profile',
        templateUrl: 'partials/profile.html',
        controller: 'ProfileCtrl',
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state('program', {
        url: '/program',
        templateUrl: 'partials/program.html',
        controller: 'ProgramCtrl',
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state('batch', {
        url: '/batch',
        templateUrl: 'partials/batch.html',
        controller: 'BatchCtrl',
        resolve: {
          loginRequired: loginRequired
        }
      });
    $urlRouterProvider.otherwise('/');

    /**
     *  Satellizer config
     */
    $authProvider.facebook({
      clientId: '603122136500203'
    });

    $authProvider.google({
      clientId: '572718978294-t22oh03u5m3toag8gu152chj4m4dn23q.apps.googleusercontent.com'
    });

    $authProvider.github({
      clientId: 'YOUR_GITHUB_CLIENT_ID'
    });

    $authProvider.linkedin({
      clientId: 'YOUR_LINKEDIN_CLIENT_ID'
    });

    $authProvider.instagram({
      clientId: 'YOUR_INSTAGRAM_CLIENT_ID'
    });

    $authProvider.yahoo({
      clientId: 'YOUR_YAHOO_CLIENT_ID'
    });

    $authProvider.live({
      clientId: 'YOUR_MICROSOFT_CLIENT_ID'
    });

    $authProvider.twitch({
      clientId: 'YOUR_TWITCH_CLIENT_ID'
    });

    $authProvider.bitbucket({
      clientId: 'YOUR_BITBUCKET_CLIENT_ID'
    });

    $authProvider.spotify({
      clientId: 'YOUR_SPOTIFY_CLIENT_ID'
    });

    $authProvider.twitter({
      url: '/auth/twitter'
    });

    $authProvider.oauth2({
      name: 'foursquare',
      url: '/auth/foursquare',
      clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
      redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
      authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate'
    });

    $authProvider.baseUrl = window.location.host === 'localhost:88'
      ? '/other/riska/pab-app/portal/'
      : '/portal/';
  });
