<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProgramDesc
 *
 * @property string $tag
 * @property string $title
 * @property string|null $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProgramDesc whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProgramDesc whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProgramDesc whereTitle($value)
 * @mixin \Eloquent
 */
class ProgramDesc extends Model
{
  protected $table = 'program_desc';
}
