<?php namespace App\Http\Controllers;

use App\District;
use App\Province;
use App\Regency;
use App\Village;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Config;
use Mail;
use Validator;
use Firebase\JWT\JWT;
use App\User;

class CommonController extends Controller
{

  /**
   * Generate JSON Web Token.
   */
  protected function createToken($user)
  {
    $payload = [
      'sub' => $user->id,
      'iat' => time(),
      'exp' => time() + (2 * 7 * 24 * 60 * 60)
    ];
    return JWT::encode($payload, Config::get('app.token_secret'));
  }

  public function getCities(Request $request)
  {
    $query = $request->input('q');

    $provinces = Province::where('name', 'like', "%{$query}%")->limit(10)->get();
    $regencies = Regency::where('name', 'like', "%{$query}%")->limit(10)->get();
//    $districts = District::where('name', 'like', "%{$query}%")->limit(10)->get();
//    $villages = Village::where('name', 'like', "%{$query}%")->limit(10)->get();

    $results = [];
    $hashMap = [];

    foreach ($provinces as $province) {
      $regency = $province->regencies->first();
      $district = $regency->districts->first();
      $village = $district->villages->first();
      if(!isset($hashMap[$province->id][$regency->id][$district->id][$village->id])) {
        $hashMap[$province->id][$regency->id][$district->id][$village->id] = true;
        $province->name = ucwords(strtolower($province->name));
        $regency->name = ucwords(strtolower($regency->name));
        $district->name = ucwords(strtolower($district->name));
        $village->name = ucwords(strtolower($village->name));
        $results[] = "{$province->name}, {$regency->name}, {$district->name}, {$village->name}";
      }
    }

    foreach ($regencies as $regency) {
      $district = $regency->districts->first();
      $village = $district->villages->first();
      $province = $regency->province;
      if(!isset($hashMap[$province->id][$regency->id][$district->id][$village->id])) {
        $hashMap[$province->id][$regency->id][$district->id][$village->id] = true;
        $province->name = ucwords(strtolower($province->name));
        $regency->name = ucwords(strtolower($regency->name));
        $district->name = ucwords(strtolower($district->name));
        $village->name = ucwords(strtolower($village->name));
        $results[] = "{$province->name}, {$regency->name}, {$district->name}, {$village->name}";
      }
    }

//    foreach ($districts as $district) {
//      $village = $district->villages->first();
//      $regency = $district->regency;
//      $province = $regency->province;
//      if(!isset($hashMap[$province->id][$regency->id][$district->id][$village->id])) {
//        $hashMap[$province->id][$regency->id][$district->id][$village->id] = true;
//        $province->name = ucwords(strtolower($province->name));
//        $regency->name = ucwords(strtolower($regency->name));
//        $district->name = ucwords(strtolower($district->name));
//        $village->name = ucwords(strtolower($village->name));
//        $results[] = "{$province->name}, {$regency->name}, {$district->name}, {$village->name}";
//      }
//    }
//
//    foreach ($villages as $village) {
//      $district = $village->district;
//      $regency = $district->regency;
//      $province = $regency->province;
//      if(!isset($hashMap[$province->id][$regency->id][$district->id][$village->id])) {
//        $hashMap[$province->id][$regency->id][$district->id][$village->id] = true;
//        $province->name = ucwords(strtolower($province->name));
//        $regency->name = ucwords(strtolower($regency->name));
//        $district->name = ucwords(strtolower($district->name));
//        $village->name = ucwords(strtolower($village->name));
//        $results[] = "{$province->name}, {$regency->name}, {$district->name}, {$village->name}";
//      }
//    }

    return $results;

  }

  public function getProvinces(Request $request)
  {
    $query = $request->input('q');
    $provinces = Province::where('name', 'like', "%{$query}%")->limit(10)->get();
    return $provinces;
  }

  public function getRegencies(Request $request)
  {
    $query = $request->input('q');
    $parent = $request->input('p');
    $regencies = Regency::where('name', 'like', "%{$query}%");
    if(!empty($parent)) $regencies = $regencies->whereProvinceId($parent);
    return $regencies->limit(10)->get();
  }

  public function getDistricts(Request $request)
  {
    $query = $request->input('q');
    $parent = $request->input('p');
    $districts = District::where('name', 'like', "%{$query}%");
    if(!empty($parent)) $districts = $districts->whereRegencyId($parent);
    return $districts->limit(10)->get();
  }

  public function getVillages(Request $request)
  {
    $query = $request->input('q');
    $parent = $request->input('p');
    $villages = Village::where('name', 'like', "%{$query}%");
    if(!empty($parent)) $villages = $villages->whereDistrictId($parent);
    return $villages->limit(10)->get();
  }

}
