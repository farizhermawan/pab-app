<?php namespace App\Http\Controllers;

use App\Mail\ConfirmationCode;
use App\Regency;
use App\RiskaBatch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Config;
use Mail;
use Validator;
use Firebase\JWT\JWT;
use App\User;

class UserController extends Controller
{

  /**
   * Generate JSON Web Token.
   */
  protected function createToken($user)
  {
    $payload = [
      'sub' => $user->id,
      'iat' => time(),
      'exp' => time() + (2 * 7 * 24 * 60 * 60)
    ];
    return JWT::encode($payload, Config::get('app.token_secret'));
  }

  /**
   * Generate Confirmation Code.
   */
  protected function createConfirmationCode(User $user)
  {
    $i = 0;
    $pin = "";
    while ($i < 4) {
      //generate a random number between 0 and 9.
      $pin .= mt_rand(0, 9);
      $i++;
    }

    $user->confirmation_code = $pin;
    $user->confirmation_expiry = Carbon::now()->addMinute(30);
    $user->save();

    return $pin;
  }

  /**
   * Get signed in user's profile.
   */
  public function getUser(Request $request)
  {
    $user = User::find($request['user']['sub']);
    $user->role;
    $user->isCompletedProfile = !empty($user->birth_date) && !empty($user->gender) && (!empty($user->whatsapp) || !empty($user->phone));
    return $user;
  }

  /**
   * Update signed in user's profile.
   */
  public function updateUser(Request $request)
  {
    $user = User::find($request['user']['sub']);

    $user->displayName = $request->input('displayName');
    $user->nickName = $request->input('nickName');
    $user->gender = $request->input('gender');
    $user->whatsapp = cleanPhone($request->input('whatsapp'));
    $user->phone = cleanPhone($request->input('phone'));
    $user->birth_place = ucwords($request->input('birth_place'));
    $user->birth_date = $request->input('birth_date');
    $user->address = $request->input('address');
    $user->address_detail = $request->input('address_detail');
    $user->save();

    return $user;
  }

  public function sendConfirmationCode(Request $request)
  {
    $user = User::find($request['user']['sub']);
    $pin = $this->createConfirmationCode($user);

    Mail::to($user->email)
      ->send(new ConfirmationCode($user->displayName, $pin));

    return response()->json(['token' => $this->createToken($user)]);
  }

  /**
   * Activate user's.
   */
  public function activateUser(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'code' => 'required'
    ]);

    if ($validator->fails()) {
      return response()->json(['message' => $validator->messages()], 400);
    }

    $user = User::find($request['user']['sub']);

    if (!$user) {
      return response()->json(['message' => 'User not found'], 400);
    }
    if ($user->confirmation_code != $request->input("code")) {
      return response()->json(['message' => 'Kode aktivasi salah'], 400);
    }
    if ($user->confirmation_expiry->lessThan(Carbon::now())) {
      return response()->json(['message' => 'Kode aktivasi expired. Silahkan klik kirim ulang kode'], 400);
    }

    $user->isActive = true;
    $user->confirmed_by = 'email';
    $user->save();

    return response()->json(['token' => $this->createToken($user)]);
  }

  public function getPrograms(Request $request) {
    $batch = RiskaBatch::current();
    $user = User::find($request['user']['sub']);
    $programs = $user->assigned_programs()->get();
    $currentProgram = null;
    $isRegistered = false;
    $isNewMember = true;
    $isUploadedPayment = false;
    foreach ($programs as $program){
      if($program->group == $batch->code) {
        $isRegistered = true;
        $currentProgram = $program;
        $isUploadedPayment = ($program->pivot->status != "PENDING" && $program->pivot->status != "REJECT");
      }
      else {
        $isNewMember = false;
      }
    }
    $isCompletedProfile = !empty($user->birth_date) && !empty($user->gender) && (!empty($user->whatsapp) || !empty($user->phone));
    return [
      'batch' => $batch,
      'programs' => $programs,
      'currentProgram' => $currentProgram,
      'isCompletedProfile' => $isCompletedProfile,
      'isRegistered' => $isRegistered,
      'isNewMember' => $isNewMember,
      'isUploadedPayment' => $isUploadedPayment
    ];
  }
}
