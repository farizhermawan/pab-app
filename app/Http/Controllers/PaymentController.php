<?php namespace App\Http\Controllers;

use App\District;
use App\Program;
use App\ProofPayment;
use App\Province;
use App\Regency;
use App\Village;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Config;
use Mail;
use Validator;
use Firebase\JWT\JWT;
use App\User;

class PaymentController extends Controller
{

  /**
   * Generate JSON Web Token.
   */
  protected function createToken($user)
  {
    $payload = [
      'sub' => $user->id,
      'iat' => time(),
      'exp' => time() + (2 * 7 * 24 * 60 * 60)
    ];
    return JWT::encode($payload, Config::get('app.token_secret'));
  }

  public function upload(Request $request)
  {
    $user = User::find($request['user']['sub']);

    $file = $request->file('file');
    $dir = "uploads";
    $filename = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
    $file->move($dir, $filename);

    $path = $dir . "/" . $filename;

    $payment = new ProofPayment();
    $payment->user_id = $user->id;
    $payment->program_id = $request->input('program_id');
    $payment->amount = $request->input('amount');
    $payment->method = "TRF";
    $payment->att_path = $path;
    $payment->payment_date = Carbon::now()->toDateString();
    $payment->save();

    $program = Program::whereId($payment->program_id)->first();

    if($user->gender == "m") $program->counter_ikhwan++;
    else $program->counter_akhwat++;
    $program->save();

    $program->participants()->updateExistingPivot($user->id, ['status' => 'WAITING']);

    return response()->json(['token' => $this->createToken($user)]);
  }

}
