<?php namespace App\Http\Controllers;

use File;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class HomeController extends Controller {

    public function index()
    {
      try {
        return File::get(public_path() . '/index.html');
      } catch (FileNotFoundException $e) {
        return response()->setStatusCode(404);
      }
    }

}