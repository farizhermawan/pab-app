<?php namespace App\Http\Controllers;

use App\District;
use App\Program;
use App\ProgramDesc;
use App\Province;
use App\Regency;
use App\RiskaBatch;
use App\Village;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Config;
use Mail;
use Validator;
use Firebase\JWT\JWT;
use App\User;

class RiskaBatchController extends Controller
{

  /**
   * Generate JSON Web Token.
   */
  protected function createToken($user)
  {
    $payload = [
      'sub' => $user->id,
      'iat' => time(),
      'exp' => time() + (2 * 7 * 24 * 60 * 60)
    ];
    return JWT::encode($payload, Config::get('app.token_secret'));
  }

  public function getAllBatch()
  {
    $batchs = RiskaBatch::get();
    return $batchs;
  }

  public function getOpenBatch()
  {
    $batchs = RiskaBatch::get();
    return $batchs;
  }

  public function getBatchPrograms($code) {
    if($code == "current") $code = RiskaBatch::current()->code;
    $batch = RiskaBatch::whereCode($code)->get()->first();
    $batch->programs = Program::whereGroup($code)->with("Detail")->get();

    return $batch;
  }

  public function assignProgram(Request $request) {
    $user = User::find($request['user']['sub']);
    $program_id = $request->input('id');

    $programs_id = [];
    $programs = Program::whereGroup(RiskaBatch::current()->code)->get(['id']);
    foreach ($programs as $id) $programs_id[] = $id;

    if ($user->assigned_programs->whereIn('program_id', $programs_id)->count() > 0) {
      return response()->json(['message' => 'Kamu tidak bisa mendaftar lebih dari satu program'], 400);
    }

    $user->assigned_programs()->attach([$program_id => ['status' => 'PENDING']]);

    return response()->json(['token' => $this->createToken($user)]);
  }
}
