<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// OAuth, Login and Signup Routes.
Route::post('auth/twitter', 'AuthController@twitter');
Route::post('auth/facebook', 'AuthController@facebook');
Route::post('auth/foursquare', 'AuthController@foursquare');
Route::post('auth/instagram', 'AuthController@instagram');
Route::post('auth/github', 'AuthController@github');
Route::post('auth/google', 'AuthController@google');
Route::post('auth/linkedin', 'AuthController@linkedin');
Route::post('auth/login', 'AuthController@login');
Route::post('auth/signup', 'AuthController@signup');
Route::get('auth/unlink/{provider}', ['middleware' => 'auth', 'uses' => 'AuthController@unlink']);

// API Routes.
Route::get('api/me', ['middleware' => 'auth', 'uses' => 'UserController@getUser']);
Route::put('api/me', ['middleware' => 'auth', 'uses' => 'UserController@updateUser']);
Route::put('api/me/activate', ['middleware' => 'auth', 'uses' => 'UserController@activateUser']);
Route::get('api/me/sendcode', ['middleware' => 'auth', 'uses' => 'UserController@sendConfirmationCode']);
Route::get('api/me/programs', ['middleware' => 'auth', 'uses' => 'UserController@getPrograms']);

Route::get('api/cities', 'CommonController@getCities');
Route::get('api/villages', 'CommonController@getVillages');
Route::get('api/districts', 'CommonController@getDistricts');
Route::get('api/regencies', 'CommonController@getRegencies');
Route::get('api/provinces', 'CommonController@getProvinces');

Route::get('api/batch', 'RiskaBatchController@getOpenBatch');
Route::get('api/batch/open', 'RiskaBatchController@getOpenBatch');
Route::get('api/batch/all', 'RiskaBatchController@getAllBatch');
Route::post('api/batch/assign', ['middleware' => 'auth', 'uses' => 'RiskaBatchController@assignProgram']);
Route::get('api/batch/{code}/programs', 'RiskaBatchController@getBatchPrograms');

Route::post('api/payment/upload', ['middleware' => 'auth', 'uses' => 'PaymentController@upload']);

// Initialize Angular.js App Route.
Route::get('/', 'HomeController@index');