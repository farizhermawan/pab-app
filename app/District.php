<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\District
 *
 * @property int $id
 * @property string $regency_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District whereRegencyId($value)
 * @mixin \Eloquent
 * @property-read \App\Regency $regency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Village[] $villages
 */
class District extends Model
{
  protected $table = 'districts';

  public function regency()
  {
    return $this->belongsTo('App\Regency');
  }

  public function villages()
  {
    return $this->hasMany('App\Village');
  }
}
