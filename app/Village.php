<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Village
 *
 * @property int $id
 * @property string $district_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Village whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Village whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Village whereName($value)
 * @mixin \Eloquent
 * @property-read \App\District $district
 */
class Village extends Model
{
  protected $table = 'villages';

  public function district()
  {
    return $this->belongsTo('App\District', 'district_id', 'id');
  }
}
