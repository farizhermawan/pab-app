<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\User
 *
 * @property int $id
 * @property string|null $email
 * @property string|null $password
 * @property string|null $displayName
 * @property string|null $facebook
 * @property string|null $foursquare
 * @property string|null $instagram
 * @property string|null $github
 * @property string|null $google
 * @property string|null $linkedin
 * @property string|null $twitter
 * @property string|null $picture
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFoursquare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGithub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGoogle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLinkedin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $isActive
 * @property string $confirmation_code
 * @property \Carbon\Carbon $confirmation_expiry
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereConfirmationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereConfirmationExpiry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsActive($value)
 * @property string|null $confirmed_by
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereConfirmedBy($value)
 * @property string|null $nickName
 * @property string|null $whatsapp
 * @property string|null $phone
 * @property string|null $brith_place
 * @property string|null $brith_date
 * @property string|null $address
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBrithDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBrithPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereWhatsapp($value)
 * @property string|null $gender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGender($value)
 * @property string|null $birth_place
 * @property \Carbon\Carbon|null $birth_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBirthPlace($value)
 * @property string|null $province
 * @property string|null $regency
 * @property string|null $district
 * @property string|null $village
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRegency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVillage($value)
 * @property string|null $address_detail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddressDetail($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Program[] $assigned_programs
 * @property int $role_id
 * @property-read \App\Role $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 */
class User extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
  protected $dates = [
    'created_at',
    'updated_at',
    'birth_date',
    'confirmation_expiry'
  ];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'confirmation_code', 'confirmation_expiry', 'confirmed_by'];

  public function assigned_programs()
  {
    return $this->belongsToMany('App\Program', 'assign_program')->withPivot(['additional_info', 'status']);
  }

  public function role()
  {
    return $this->belongsTo('App\Role');
  }
}
