<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Regency
 *
 * @property int $id
 * @property string $province_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Regency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Regency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Regency whereProvinceId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\District[] $districts
 * @property-read \App\Province $province
 */
class Regency extends Model
{
  protected $table = 'regencies';

  public function province()
  {
    return $this->belongsTo('App\Province');
  }

  public function districts()
  {
    return $this->hasMany('App\District');
  }
}
