<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmationCode extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($name, $pin)
  {
    $this->name = $name;
    $this->pin = $pin;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->view('mail.confirm-code')
      ->from("pab@riska.or.id", "PAB RISKA")
      ->subject("Kode Aktifasi | PAB RISKA")
      ->with([
        'user' => $this->name,
        'code' => $this->pin
      ]);
  }
}
