<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\RiskaBatch
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $open_date
 * @property string $close_date
 * @property string $prefix_reg_code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Program[] $programs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RiskaBatch whereCloseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RiskaBatch whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RiskaBatch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RiskaBatch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RiskaBatch whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RiskaBatch whereOpenDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RiskaBatch wherePrefixRegCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RiskaBatch whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RiskaBatch extends Model
{
  protected $table = 'riska_batch';

  public function programs()
  {
    return $this->hasMany('App\Program', 'group', 'code');
  }

  public static function current()
  {
    $now = Carbon::now();
    return RiskaBatch::whereDate('open_date', '<', $now)->whereDate('close_date', '>', $now)->get()->first();
  }
}
