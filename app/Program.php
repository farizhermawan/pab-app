<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Program
 *
 * @property int $id
 * @property string $group
 * @property string $tag
 * @property string $name
 * @property int $quota
 * @property int|null $quota_ikhwan
 * @property int|null $quota_akhwat
 * @property int $counter_ikhwan
 * @property int $counter_akhwat
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\RiskaBatch $riskaBatch
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereCounterAkhwat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereCounterIkhwan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereQuota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereQuotaAkhwat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereQuotaIkhwan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Program whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\ProgramDesc $detail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $participants
 */
class Program extends Model
{
  protected $table = 'programs';

  public function riskaBatch()
  {
    return $this->belongsTo('App\RiskaBatch', 'group', 'code');
  }

  public function participants()
  {
    return $this->belongsToMany('App\User', 'assign_program')->withPivot(['status', 'additional_info']);
  }

  public function detail()
  {
    return $this->belongsTo('App\ProgramDesc', 'tag', 'tag');
  }

}
