<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Role
 *
 * @property int $id
 * @property string $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereRole($value)
 * @mixin \Eloquent
 */
class Role extends Model
{
  protected $table = 'roles';
  public $timestamps = false;

  public function users()
  {
    return $this->hasMany('App\User');
  }
}
