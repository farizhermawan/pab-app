<?php

if (! function_exists('cleanPhone')) {
  function cleanPhone($str) {
    if (empty($str)) return null;
    $countryCode = env('COUNTRY_CODE', '62');
    $str = str_replace(["-", "+", " "], "", $str);
    if (substr($str, 0, strlen($countryCode)) == $countryCode) $str = "0" . substr($str, strlen($countryCode));
    return $str;
  }
}
