<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProofPayment
 *
 * @property int $id
 * @property int $user_id
 * @property int $program_id
 * @property int $amount
 * @property string $method
 * @property string $att_path
 * @property \Carbon\Carbon $payment_date
 * @property string $confirm_by
 * @property \Carbon\Carbon $confirm_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment whereAttPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment whereConfirmBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment whereConfirmDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment whereProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProofPayment whereUserId($value)
 * @mixin \Eloquent
 */
class ProofPayment extends Model
{
  protected $table = 'proof_payments';
  protected $dates = [
    'payment_date',
    'confirm_date'
  ];

  public $timestamps = false;

}
