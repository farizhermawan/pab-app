<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfirmationByEmail extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->boolean("isActive");
      $table->string("confirmation_code", 6)->nullable();
      $table->string("confirmed_by", 10)->nullable();
      $table->timestamp("confirmation_expiry")->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn(['isActive', 'confirmation_code', 'confirmed_by', 'confirmation_expiry']);
    });
  }
}
