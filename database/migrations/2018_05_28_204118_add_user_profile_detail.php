<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserProfileDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->string("nickName", 10)->nullable();
          $table->string("gender", 1)->nullable();
          $table->string("whatsapp", 15)->nullable();
          $table->string("phone", 15)->nullable();
          $table->string("birth_place", 20)->nullable();
          $table->date("birth_date")->nullable();
          $table->text("address")->nullable();
          $table->text("address_detail")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['nickName', 'gender', 'whatsapp', 'phone', 'birth_place', 'birth_date', 'address', 'address_detail']);
        });
    }
}
