<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableProofPayment extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('proof_payments', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id');
      $table->integer('program_id');
      $table->integer('amount');
      $table->string('method');
      $table->string('att_path');
      $table->date('payment_date');
      $table->string('confirm_by');
      $table->date('confirm_date');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('proof_payments');
  }
}
