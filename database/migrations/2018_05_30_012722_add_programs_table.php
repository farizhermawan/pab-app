<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProgramsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('programs', function (Blueprint $table) {
      $table->increments('id');
      $table->string('group', 5);
      $table->string('tag', 5);
      $table->string('name');
      $table->integer('quota');
      $table->integer('quota_ikhwan')->nullable();
      $table->integer('quota_akhwat')->nullable();
      $table->integer('counter_ikhwan')->default(0);
      $table->integer('counter_akhwat')->default(0);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('programs');
  }
}
