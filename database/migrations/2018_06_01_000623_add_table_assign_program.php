<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableAssignProgram extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('assign_program', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id');
      $table->integer('program_id');
      $table->string('status', 10);
      $table->string('additional_info');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('assign_program');
  }
}
