<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRiskaBatchTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('riska_batch', function (Blueprint $table) {
      $table->increments('id');
      $table->string('code', 5);
      $table->string('name');
      $table->date('open_date');
      $table->date('close_date');
      $table->string('prefix_reg_code', 3);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('riska_batch');
  }
}
